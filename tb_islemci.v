`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.10.2021 22:43:20
// Design Name: 
// Module Name: tb_islemci
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_islemci(

    );
    reg clk=0;
    reg rst=0;
    reg [31:0] buyruk;
    wire [31:0] ps;
    wire [31:0] yazmac_on;
    islemci uut(clk,rst,buyruk,ps,yazmac_on);
    always begin
        clk=~clk;
        #5;
    end
    
    initial begin
        buyruk=32'b000000000010_00101_000_01100_0010011;
        #10;
        buyruk=32'b000000000010_00101_000_01011_0010011;
        #10;
        buyruk=32'b0_0000000001_0_00000000_01010_1101111;
    end
endmodule
