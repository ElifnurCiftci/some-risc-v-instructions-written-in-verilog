# Some RISC-V instructions written in Verilog

Basic implementation of some 32 bit risc-v instructions on Verilog.

Implemented instructions are JAL, JALR, BEQ, LW, SW, ADDI, SLTI, XORI, SRAI, ADD, MUL, SUB, XOR, SRL, SRA, AND.

The code inputs are clock, reset, instruction respectively.
Outputs are ps(program counter), tenth_register respectively.

A testbench is included.
