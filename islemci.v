`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.02.2022 02:30:07
// Design Name: 
// Module Name: islemci
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module islemci(
    input saat,
    input reset,
    input [31:0] buyruk,
    output reg [31:0] ps,
    output reg [31:0] yazmac_on 
    );
    
    reg [31:0] ps;
    reg signed [31:0] yazmac_obegi [31:0];
    reg signed [31:0] mem [31:0];
    reg [4:0] rd, rs1, rs2, shamt;
    reg signed [19:0] s_imm20; 
    reg [2:0] func3;
    reg [6:0] opcode, func7, imm7;
    reg signed [11:0] s_imm12;
    reg signed [31:0] I_imm;
    reg signed [31:0] J_imm;
    reg signed [31:0] B_imm;
    reg signed [31:0] S_imm;
    
    
    integer i; 
    
    initial begin
       for (i = 0; i<32 ; i = i+1) begin
            yazmac_obegi[i] = 32'b0000_0000_0000_0000_0000_0000_0000_0000;
            mem[i] = 32'b0000_0000_0000_0000_0000_0000_0000_0000;
       end
       ps = 32'h0000_0000;
       ps = ps-4;
    end
 
    always@(posedge saat) begin
        if(reset) begin
            ps = 32'h0000_0000;
            ps = ps-4;
            for (i = 0; i<32 ; i = i+1) begin
                yazmac_obegi[i] = 32'b0000_0000_0000_0000_0000_0000_0000_0000;
            end
        end
        else begin
            opcode  = buyruk[6:0];
            if(opcode == 7'b1101111)begin //JAL
                rd = buyruk[11:7];
                yazmac_obegi[rd] = ps+4;
                s_imm20 = {buyruk[31],buyruk[19:12],buyruk[20],buyruk[30:21]};
                J_imm = {s_imm20,12'd0};
                J_imm = J_imm >>> 11;
                ps = ps + J_imm;
            end
            else if (opcode == 7'b1100111)begin
                func3 = buyruk[14:12];
                if(func3 == 3'b000)begin //JALR
                    rs1 = buyruk[19:15];
                    rd = buyruk[11:7];
                    s_imm12 = buyruk[31:20];
                    I_imm = {s_imm12,20'd0};
                    I_imm = I_imm >>> 20;
                    yazmac_obegi[rd]=ps+4;
                    ps=(yazmac_obegi[rs1]+I_imm)&~1;
                end
            end
            else if(opcode == 7'b1100011)begin
                func3 = buyruk[14:12];
                rs1 = buyruk[19:15];
                rs2 = buyruk[24:20];
                s_imm12 = {buyruk[31],buyruk[7],buyruk[30:25],buyruk[11:8]};
                B_imm = {s_imm12,20'd0};
                B_imm = B_imm >>> 19;
                if(func3 == 3'b000)begin //BEQ
                    ps = (yazmac_obegi[rs1] == yazmac_obegi[rs2]) ? ps+B_imm : ps+4;
                end
                else if(func3 == 3'b100)begin //BLT
                    ps = (yazmac_obegi[rs1] < yazmac_obegi[rs2]) ? ps+B_imm : ps+4;
                end
                else if(func3 == 3'b101)begin //BGE
                    ps = (yazmac_obegi[rs1] >= yazmac_obegi[rs2]) ? ps+B_imm : ps+4;
                end
            end
            else if(opcode == 7'b0000011)begin //LW
                rs1 = buyruk[19:15];
                rd = buyruk[11:7];
                s_imm12 = buyruk[31:20];
                I_imm = {s_imm12,20'd0};
                I_imm = I_imm >>> 20;
                yazmac_obegi[rd] = mem[S_imm + yazmac_obegi[rs1]];
            end
            else if(opcode == 7'b0100011)begin //SW
                rs1 = buyruk[19:15];
                rs2 = buyruk[24:20];
                s_imm12 = {buyruk[31:25],buyruk[11:8],buyruk[7]};
                S_imm = {s_imm12,20'd0};
                S_imm = S_imm >>> 20;
                mem[S_imm + yazmac_obegi[rs1]] = yazmac_obegi[rs2];
            end
            else if(opcode == 7'b0010011)begin
                func3 = buyruk[14:12];
                rs1 = buyruk[19:15];
                rd = buyruk[11:7];
                s_imm12 = buyruk[31:20];
                I_imm = {s_imm12,20'd0};
                I_imm = I_imm >>> 20;
                if(func3 == 3'b000) begin //ADDI
                    yazmac_obegi[rd] = I_imm + yazmac_obegi[rs1];
                end
                else if(func3 == 3'b010) begin //SLTI
                    yazmac_obegi[rd] = yazmac_obegi[rs1] < I_imm; 
                end
                else if(func3 == 3'b100) begin //XORI
                    yazmac_obegi[rd] = yazmac_obegi[rs1] ^ I_imm;
                end
                else if(func3 == 3'b101) begin 
                    imm7 = buyruk[31:25];
                    shamt = buyruk[24:20];
                    if(imm7 == 7'b0100000)begin //SRAI
                        yazmac_obegi[rd] = yazmac_obegi[rs1] >>> shamt;
                    end
                end
                ps=ps+4;
             end
             else if(opcode == 7'b0110011)begin
                func7 = buyruk[31:25];
                func3 = buyruk[14:12];
                rs1 = buyruk[19:15];
                rs2 = buyruk[24:20];
                rd = buyruk[11:7];
                if(func3 == 3'b000)begin
                    if(func7 == 7'b0000000)begin //ADD
                        yazmac_obegi[rd] = yazmac_obegi[rs1] + yazmac_obegi[rs2];
                    end
                    else if(func7 == 7'b0000001)begin //MUL
                        yazmac_obegi[rd] = yazmac_obegi[rs1] * yazmac_obegi[rs2];
                    end
                    else if(func7 == 7'b0100000)begin //SUB
                        yazmac_obegi[rd] = yazmac_obegi[rs2] - yazmac_obegi[rs1];
                    end
                end
                else if(func3 == 3'b100)begin 
                    if(func7 == 7'b0000000)begin //XOR
                       yazmac_obegi[rd] = yazmac_obegi[rs1] ^ yazmac_obegi[rs2];
                    end
                end
                else if(func3 == 3'b101)begin
                    if(func7 == 7'b0000000)begin //SRL
                        yazmac_obegi[rd] = yazmac_obegi[rs1] >> yazmac_obegi[rs2];
                    end
                    if(func7 == 7'b0100000)begin //SRA
                        yazmac_obegi[rd] = yazmac_obegi[rs1] >>> yazmac_obegi[rs2];
                    end
                end
                else if(func3 == 3'b111)begin 
                    if(func7 == 7'b0000000)begin //AND
                        yazmac_obegi[rd] = yazmac_obegi[rs1] & yazmac_obegi[rs2];
                    end
                end
                ps = ps+4;
             end
        end
        yazmac_obegi[0]=0;
        yazmac_on=yazmac_obegi[10];
    end
    
endmodule
